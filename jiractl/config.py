from pathlib import Path

import yaml


class AutomaticMrStatus:
    def __init__(self, current_project, config_projects, path=None):
        self.project_name = current_project
        if path is None:
            path = Path.home() / ".config/jiractl_automatic_mr.yaml"
        try:
            with open(path, "r") as yaml_file:
                self._config = yaml.load(yaml_file, Loader=yaml.FullLoader)
        except FileNotFoundError:
            self._config = {
                list(name.keys())[0] + '_last_iid': None for name in config_projects}

    def get_last_iid(self):
        return self._config.get(self.project_name + '_last_iid', None)

    def set_last_iid(self, iid):
        self._config[self.project_name + '_last_iid'] = iid

    def write_into_file(self, path=None):
        if path is None:
            path = Path.home() / ".config/jiractl_automatic_mr.yaml"
        with open(path, "w") as yaml_file:
            yaml.dump(self._config, yaml_file)


class Config:

    def __init__(self, path=None):
        """Read configuration from YAML file"""
        if path is None:
            path = Path.home() / ".config/jiractl.yaml"
        with open(path, "r") as yaml_file:
            self._config = yaml.load(yaml_file, Loader=yaml.FullLoader)

    def get_project(self, project=None):
        for project_info in self._config["projects"]:
            name = list(project_info)[0]
            if project is None or name == project:
                return Project(project_info[name])

    @property
    def jira_server(self):
        return self._config['jira']['server']

    @property
    def jira_credentials(self):
        return self._config['jira']['user'], self._config['jira'].get('pass', None)

    @property
    def github_token(self):
        return self._config['github']['token']

    @property
    def gitlab_token(self):
        return self._config['gitlab']['token']

    @property
    def projects_list(self):
        return self._config['projects']


class Project:

    def __init__(self, config):
        self._config = config

    @property
    def repo(self):
        return self._config['repo']

    @property
    def platform(self):
        return self._config['platform']

    @property
    def jira_project(self):
        return JiraProject(self._config['jira'])

    @property
    def ignored_users(self):
        # TODO: this needs to be added to the readme.
        return self._config.get('ignored_users', [])


class JiraProject:

    def __init__(self, config):
        self._config = config

    @property
    def project(self):
        return self._config['project']

    @property
    def issue_type(self):
        return self._config['issue_type']

    @property
    def component(self):
        return self._config['component']

    @property
    def customer_unit(self):
        return self._config['customer_unit']

    @property
    def transition(self):
        return self._config.get('transition', None)

    @property
    def epic_link(self):
        return self._config.get('epic_link', None)
