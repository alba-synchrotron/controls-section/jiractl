from abc import ABC, abstractmethod


class VCPlatform(ABC):
    """Abstract base class for Version Control platform interaction."""

    @abstractmethod
    def get_item_info(self, repo, nb):
        """Retrieve issue or pull/merge request information."""
        pass

    @abstractmethod
    def get_open_new_mrs_since_date(self, repo, from_day=None, ignored_users=[]):
        """Retrieve open PRs/MRs created after a specific date."""
        pass

    @abstractmethod
    def get_mr_by_iid(self, repo, iid):
        """Retrieve a specific PR/MR by its ID."""
        pass

    @abstractmethod
    def get_mr_created_at(self, mr):
        """Return the creation datetime of a given merge request/pull request as a datetime object."""
        pass
