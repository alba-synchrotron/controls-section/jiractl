import logging
import click
from jiractl.config import Config, AutomaticMrStatus
from jiractl.jira import Jira
from jiractl.gitlab import Gitlab
from jiractl.github import Github


def get_vc_platform(project, config):
    """Factory method to get the correct VC platform instance."""
    if project.platform == "gitlab":
        return Gitlab(token=config.gitlab_token)
    elif project.platform == "github":
        return Github(token=config.github_token)
    else:
        raise ValueError(f'Unknown platform "{project.platform}"')


@click.group('jiractl')
def jiractl():
    pass


@jiractl.command()
@click.option('--project',
              type=click.STRING,
              help='Source project from the configuration file')
@click.argument('nb', type=click.STRING)
def dup(nb, project):
    """Import issue from a VC platform (GitLab/GitHub) to JIRA.

    Project information must be configured in ~/.config/jiractl.yaml
    """
    config = Config()
    project_cfg = config.get_project(project)
    platform = get_vc_platform(project_cfg, config)
    issue_dict = platform.get_item_info(project_cfg.repo, nb)

    jira = Jira(server=config.jira_server,
                credentials=config.jira_credentials)
    jira_project = project_cfg.jira_project
    jira_project_fields = Jira.get_issue_fields(jira_project)
    issue_dict.update(jira_project_fields)
    issue = jira.create_issue(issue_dict)
    print('Issue URL:', issue.permalink())

    transition = jira_project.transition
    if transition is not None:
        jira.transition_issue(issue, transition)


@jiractl.command()
@click.option('--project',
              required=True,
              type=click.STRING,
              help='source project from the configuration file')
def automatic_mr(project):
    """Import all merge request from a VC platform (GitLab/GitHub) to JIRA

    Projects information must be configured in ~/.config/jiractl.yaml
    """
    logging.basicConfig(level=logging.INFO, format='%(asctime)s %(message)s')
    config = Config()
    project_cfg = config.get_project(project)
    project_str = str(project).upper()
    platform = get_vc_platform(project_cfg, config)
    logging.info(f"{project_str} Script start")

    status = AutomaticMrStatus(
        current_project=project, config_projects=config.projects_list)

    # Retrieve the last processed MR/PR ID
    last_iid = status.get_last_iid()

    if last_iid is None:
        logging.info(
            f"{project_str} Cannot get the last created Jira issue. Saving the last MR/PR on GitLab/GitHub.")
        _, iids = platform.get_open_new_mrs_since_date(project_cfg.repo)
        if iids:
            status.set_last_iid(iids[0])
            status.write_into_file()
        logging.info(f"{project_str} Script finish")
        return

    # Fetch last processed MR/PR
    try:
        last_mr = platform.get_mr_by_iid(project_cfg.repo, last_iid)
    except ValueError:
        logging.error(
            f"{project_str} Cannot get the last MR/PR with iid {last_iid} from GitLab/GitHub. Check that the iid is valid.")
        return
    last_mr_datetime = platform.get_mr_created_at(last_mr)
    from_date = last_mr_datetime.strftime('%Y%m%d')

    logging.info(f"{project_str} Fetching new MRs/PRs from {from_date}.")

    # Get new MRs/PRs since the last processed one
    mr_list, mr_iids = platform.get_open_new_mrs_since_date(
        project_cfg.repo, from_day=from_date, ignored_users=project_cfg.ignored_users
    )
    if not mr_list:
        logging.info(f"{project_str} No new opened MRs/PRs.")
        logging.info(f"{project_str} Script finish")
        return

    logging.info(f"{project_str} List of opened MR/PR IDs: {mr_iids}")

    jira = Jira(server=config.jira_server,
                credentials=config.jira_credentials)
    jira_project = project_cfg.jira_project
    jira_project_fields = Jira.get_issue_fields(jira_project)

    for mr, iid in zip(reversed(mr_list), reversed(mr_iids)):
        if iid > last_iid:  # Only process if it's a new MR/PR
            mr.update(jira_project_fields)
            logging.info(f"Creating issue: {mr}")
            issue = jira.create_issue(mr)

            # Apply transition if it has been defined in the configuration file
            transition = jira_project.transition or "In Review"
            logging.info(f"Apply transition: {transition}")
            jira.transition_issue(issue, transition)

            logging.info(f"{project_str} Issue URL: {issue.permalink()}")
        else:
            logging.info(
                f"{project_str} Ignoring MR/PR {iid}, Jira issue already created.")

    # Update the last processed MR/PR ID
    status.set_last_iid(mr_iids[0])
    status.write_into_file()

    logging.info(f"{project_str} Script finish")
