import logging
from datetime import datetime
from gitlab import Gitlab as Gitlab_
from jiractl.vc_platform import VCPlatform


class Gitlab(VCPlatform):

    MR_PREFIX = "!"
    ISSUE_PREFIX = "#"

    def __init__(self, token):
        self.gitlab = Gitlab_(url="https://gitlab.com", private_token=token)
        logging.basicConfig(level=logging.INFO,
                            format='%(asctime)s %(message)s')

    def get_item_info(self, repo, nb):
        """Get issue/MR info from GitLab"""
        item_prefix = nb[0]
        nb_wo_prefix = nb[1:]
        assert item_prefix in (Gitlab.MR_PREFIX, Gitlab.ISSUE_PREFIX), \
            "GitLab numbers must start with '!' or '#'"

        repo = self.gitlab.projects.get(repo)
        try:
            item = repo.issues.get(
                nb_wo_prefix) if item_prefix == Gitlab.ISSUE_PREFIX else repo.mergerequests.get(nb_wo_prefix)
        except Exception as e:
            raise ValueError(
                f"{'Issue' if item_prefix == Gitlab.ISSUE_PREFIX else 'MR'} with id {nb_wo_prefix} could not be retrieved") from e

        issue_dict = {
            'summary': f"{item.title} ({nb})",
            'description': item.web_url,
        }
        return issue_dict

    def get_open_new_mrs_since_date(self, repo, from_day=None, ignored_users=[]):
        repo = self.gitlab.projects.get(repo)
        try:
            if from_day is None:
                items = repo.mergerequests.list(per_page=1)
            else:
                from_day_datetime = datetime.strptime(
                    from_day, '%Y%m%d').date()
                items = repo.mergerequests.list(
                    created_after=from_day_datetime, get_all=True, state='opened')
        except Exception as e:
            raise ValueError(str(e))

        mr_list = []
        mr_iids = []
        for i in items:
            if i.author['username'] in ignored_users:
                logging.info("Ignoring MR with iid{} from author {}".format(
                    i.iid, i.author['username']))
                continue
            issue_dict = {
                'summary': f"{i.title} ({i.iid})",
                'description': i.web_url,
            }
            mr_list.append(issue_dict)
            mr_iids.append(i.iid)
        return mr_list, mr_iids

    def get_mr_by_iid(self, repo, iid):
        repo = self.gitlab.projects.get(repo)
        return repo.mergerequests.get(iid)

    def get_mr_created_at(self, mr):
        """Convert GitLab's MR created_at timestamp to a datetime object."""
        return datetime.strptime(mr.created_at, "%Y-%m-%dT%H:%M:%S.%fZ")
