from github import Github as Github_
from datetime import datetime
import logging
from jiractl.vc_platform import VCPlatform


class Github(VCPlatform):

    def __init__(self, token):
        self.github = Github_(token)
        logging.basicConfig(level=logging.INFO,
                            format="%(asctime)s %(message)s")

    def get_item_info(self, repo, nb):
        """Get issue/PR information from GitHub"""
        repo = self.github.get_repo(repo)
        try:
            issue_or_pr = repo.get_issue(nb)
        except Exception as e:
            raise ValueError(
                "issue or pr with {} could not be retrieved".format(nb)) from e
        issue_dict = {
            "summary": f"{issue_or_pr.title} ({nb})",
            "description": issue_or_pr.html_url,
        }
        return issue_dict

    def get_open_new_mrs_since_date(self, repo, from_day=None, ignored_users=[]):
        """Get open PRs created after a specific date"""
        repo = self.github.get_repo(repo)

        if from_day:
            from_day_datetime = datetime.strptime(from_day, "%Y%m%d")
        else:
            from_day_datetime = None

        try:
            open_prs = repo.get_pulls(
                state="open", sort="created", direction="desc")
        except Exception as e:
            raise ValueError(f"Error fetching PRs: {str(e)}")

        pr_list = []
        pr_numbers = []

        for pr in open_prs:
            pr_created_at = pr.created_at  # GitHub API returns datetime in UTC

            if from_day_datetime and pr_created_at.date() < from_day_datetime.date():
                break

            if ignored_users and pr.user.login in ignored_users:
                logging.info(
                    f"Ignoring PR #{pr.number} from author {pr.user.login}")
                continue

            pr_dict = {
                "summary": f"{pr.title} (#{pr.number})",
                "description": pr.html_url,
            }
            pr_list.append(pr_dict)
            pr_numbers.append(pr.number)

        return pr_list, pr_numbers

    def get_mr_by_iid(self, repo, iid):
        try:
            pr = self.github.get_repo(repo).get_pull(iid)
        except Exception as e:
            raise ValueError(
                f"PR with iid {iid} could not be retrieved") from e
        return pr

    def get_mr_created_at(self, mr):
        """Return GitHub PR created_at timestamp (already a datetime object)."""
        return mr.created_at  # GitHub's API already returns a `datetime.datetime` object.
