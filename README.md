# jiractl tools

## Features

- duplicate a GitHub/GitLab issue as a JIRA issue
- automatically create JIRA issues with new Merge Requests (MR). It only works with GitLab.

## Installation

Editable mode is recommended. This way it will be easier for you
to implement new features and hopefully provide Merge Requests :)

Being in the project directory:

```
conda env create --file environment.yml
```

or 

```
pip install -e .
```

## Configuration

Configuration is stored in `~/.config/jiractl.yaml`

In the configuration file you can have:
- multiple GitHub or GitLab source projects
  (the first one is the default one)
- each source project has only one JIRA destination project

Example of configuration file:

```yaml
github:
    token: <your_github_token>
gitlab:
    token: <your_gitlab_token>
jira:
    server: <your_jira_server_url> # e.g. https://jira.atlassian.com
    user: <your_jira_username>
    pass: <your_jira_password>
projects:
    - sardana:
        platform: gitlab
        repo: sardana-org/sardana
        jira:
            project: CSGSW
            issue_type: Story
            component: Sardana
            customer_unit: World
            transition: Planned
            epic_link: <jira_epic_key>
        ignored_users:
            - <gitlab_username>
            - <gitalb_username>
    - pyicepap:
        platform: github
        repo: ALBA-Synchrotron/pyIcePAP
        jira:
            project: CS
            issue_type: Problem
            component: Motion
            customer_unit: Beamlines
            transition: Planned
            epic_link: <jira_epic_key>
        ignored_users:
            - <gitlab_username>
            - <gitalb_username>
```

Hints:
- Issue transition names may directly map to the issue states when transitions
  are not explicitly configured. For example, in one project the _Plan_
  transition is explicitly defined and changes an issue to the _Planned_
  state, and in another project, this transition is not explicitly defined,
  hence the transition inherits its name from the _Planned_ state.
  Always consult the particular workflow configuration to obtain this
  information. 


There is an additional file stored in `~/.config/jiractl_automatic_mr.yaml`, which is generated when using the feature to automatically create JIRA issues from new Merge Requests. It contains the `iid` of the last MR whose JIRA issue has been created. Whilst there is no need to edit it manually, you can overwrite the value if you want to create issues from a given MR iid.
```yaml
sardana_last_iid: 1950
taurus_last_iid: 1260
taurus_pyqtgraph_last_iid: 108
```

  
## Usage

**IMPORTANT: To interface with ALBA's JIRA you need to be connected to the ALBA network e.g. using VPN.**

### duplicate a GitHub/GitLab issue as a JIRA issue

*Note: You will most likely need to use an escape character (e.g. `\`) to run the commands below.*

Assuming the above configuration file:

Create JIRA issue from sardana-org/sardana#1513 issue (GitLab):
```console
$> jiractl dup #1513
Issue URL: https://jira.atlassian.com/browse/CSGSW-14948
```

Create JIRA issue from sardana-org/sardana!1514 merge request (GitLab):
```console
$> jiractl dup !1514
Issue URL: https://jira.atlassian.com/browse/CSGSW-14948
```

Create JIRA issue from ALBA-Synchrotron/pyIcePAP#10 (GitHub) issue or PR:
```console
$> jiractl dup 10 --project=pyicepap
Issue URL: https://jira.atlassian.com/browse/CS-4100
```
### Automatically create JIRA issues with new MR
Create JIRA issues with new Merge Requests since last execution:
```console
$> jiractl automatic-mr --project=sardana
```

Inside the file `jiractl.yaml` there is the field `ignored_users`. It can be used to ignore new MR created by such users. This way, new Jira issues will only be created from MR outside our team.

It is meant to be run as a cron job. This is an example of a possible crontab config:
```
# m  h  dom mon dow   command

  5  4  *   *   *     /home/user/jiractl automatic-mr --project=sardana &>> /home/user/cron_output_sardana
  10 4  *   *   *     /home/user/jiractl automatic-mr --project=taurus &>> /home/user/cron_output_taurus
  15 4  *   *   *     /home/user/jiractl automatic-mr --project=taurus_pyqtgraph &>> /home/user/cron_output_taurus_pyqtgraph
```

## TODO:
- Add OAuth support for JIRA: https://jira.readthedocs.io/en/master/examples.html#oauth
- Add support to other fields: Assignee, etc.
